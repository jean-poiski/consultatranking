<?php
require_once "controller/utils.php";
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cadastro de Motoristas / Parceiros </title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Styles -->
  <link rel="stylesheet" type="text/css" media="screen" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" />
  <link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" />
  <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
  <link rel="stylesheet" type="text/css" media="screen" href="css/jquery.loading.css" />
  <link rel="stylesheet" type="text/css" media="screen" href="css/font-awesome-animation.min.css" />

  <!-- Scripts --> 
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  <script src="js/main.js"></script>
  <script src="js/jquery.loading.js"></script>

  <script type="text/javascript">
    $(document).ready(function(){
      $("#nrplaca").inputmask({mask: 'AAA-9999'});
      $("#nrcpf").inputmask({mask:'999.999.999-99'});
      $("#propnrcpf").inputmask({mask:'999.999.999-99'});
      $("#nrcep").inputmask({mask:'99.999-999'});
      $("#propnrcep").inputmask({mask:'99.999-999'});

      $("#propnrcnpj").inputmask({mask:'99.999.999/9999-99'});

      carregaCidadesEstados("ufemissaorg");
      carregaCidadesEstados("dsufnatural", "dscidadenatural");
      carregaCidadesEstados("dsuf", "dscidade");
      carregaCidadesEstados("ufcnh");
      carregaCidadesEstados("propufemissaorg");
      carregaCidadesEstados("propdsufnatural", "propdscidadenatural");
      carregaCidadesEstados("propdsuf", "propdscidade");

      carregaCidadesPermitidas();


      $("#pills-propfiscia-tab").change(function(){ console.warn(arguments); }).change()

      $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
        
        setCamposObrigatoriosProprietario(e.target.id);

      });

    });
  </script>

</head>

<body>

    <div class="jumbotron jumbotron-fluid qargo-cabecalho">
      <div class="row">
        <div class="col-md-3 col-sm-6">
        </div>

        <div class="col-md-2 col-sm-6">
          <div >
            <img src="img/logo-original-horizonta.png" class="img-responsive qargo-cabecalho mt-4" width="100%"></img>
          </div>
        </div>
        <div class="col-md-5 col-sm-6">

          <h3 class="text-center qargo-cabecalho-title">Seja um Entregador com Qargo</h3>  

          <span class="lead text-muted text-center qargo-cabecalho-text">
            <p >Aumente sua renda! </p> 
            <p>Esteja disponível para aceitar cargas, fretes, serviços fixos ou esporádicos através da Qargo.</p>
            <p>Tenha em mãos ou na galeria de fotos do seu dispositivo a sua CNH, CRLV, Comprovante de residência e a ANTT (RNTRC).</p>
            <p class="aviso-cidade-permitida">Temporariamente disponível para residentes na Capital e Região Metropolitana de São Paulo.</p>
          </span>  
        </div>

        <div class="col-md-2 col-sm-6">
        </div>


      </div>

    </div>

  <div class="container">
    
    <form id="principal" action="controller/sendform.php" method="POST" enctype="multipart/form-data">

      <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="pills-motorista-tab" data-toggle="pill" href="#pills-motorista" role="tab" aria-controls="pills-motorista" aria-selected="true">1.Motorista</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="pills-proprietario-tab" data-toggle="pill" href="#pills-proprietario" role="tab" aria-controls="pills-proprietario" aria-selected="false">2.Proprietário</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="pills-meuveiculo-tab" data-toggle="pill" href="#pills-meuveiculo" role="tab" aria-controls="pills-meuveiculo" aria-selected="false">3.Veículo</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">4.Documentos</a>
        </li>
      </ul>

    <div class="row">
        
        <div class="col-md-12 col-sm-6">
          <div class="alert alert-danger fade show" role="alert">
            Campos marcados com <span class="required"></span> são obrigatórios <i class="far fa-smile-wink"></i>
          </div>
        </div>
  
      </div>
  

      <div class="row">

        <div class="col-md-12 col-sm-6">

          <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-motorista" role="tabpanel" aria-labelledby="pills-motorista-tab">
              <div class="form-group">

                <div class="row">
                  <div class="col-md-3 col-sm-6">
                    <label for="nrcpf" class="required">CPF</label>
                    <input type="text" class="form-control" id="nrcpf" name="nrcpf" placeholder="CPF" onblur="validaCPF(this)" required>
                  </div>
                  
                  <div class="col-md-9 col-sm-6">
                    <label for="nomemotorista" class="required">Nome</label>
                    <input type="text" class="form-control" id="nomemotorista" name="nomemotorista" placeholder="Nome" required>
                  </div>
                </div>
                
                <div class ="row">
                  <div class="col-md-3 col-sm-6">
                    <label for="nrrg" class="required">RG</label>
                    <input type="text" class="form-control" id="nrrg" name="nrrg" placeholder="RG" required>
                  </div>
                  
                  <div class="col-md-3 col-sm-6">
                    <label for="dtemissaorg" class="required">Data de Emissão RG</label>
                    <input type="date" class="form-control" id="dtemissaorg" name="dtemissaorg" placeholder="Data de Emissão" required>
                  </div>
                  
                  <div class="col-md-3 col-sm-6">
                    <label for="ufemissaorg" class="required">Estado Emissão RG</label>

                    <select class='form-control' name="ufemissaorg" id="ufemissaorg" name="ufemissaorg" required> </select>
                  </div>

                  <div class="col-md-3 col-sm-6">
                    <label for="orgaoemissaorg" class="required">Orgão Emissor</label>
                    <input type="text" class='form-control' name="orgaoemissaorg" id="orgaoemissaorg" required>
                  </div>

                </div>

                <div class="row">

                    <div class="col-md-3 col-sm-6">
                      <label for="dtnasc" class="required">Data Nascimento</label>
                      <input type="date" class="form-control" id="dtnasc" name="dtnasc" placeholder="Data de Nascimento" required max="<?php echo date('Y-m-d',strtotime("-1 days")); ?>">
                    </div>

                    <div class="col-md-1 col-sm-6">
                      <label for="dscidadenatural" class="required">Natural</label>
                      <select class='form-control' name="dsufnatural" id="dsufnatural" required> </select>
                    </div>

                    <div class="col-md-8 col-sm-6">
                      <label for="dscidadenatural">&nbsp;</label>
                      <select class='form-control' name="dscidadenatural" id="dscidadenatural" data-toggle="tooltip" data-placement="top" onmouseover="toolTipCidades(this)" required> </select>
                    </div>

                </div>

                <div class="row">
                  <div class="col-md-3 col-sm-6">
                    <label for="nrcep" class="required">CEP</label>
                    <input type="text" class="form-control" id="nrcep" name="nrcep" placeholder="99.999-99" onblur="carregaDadosCep(this)" required>
                  </div>

                  <div class="col-md-9 col-sm-6">
                    <label for="dsrua" class="required">Rua</label>
                    <input type="text" class="form-control" id="dsrua" name="dsrua" placeholder="Rua" required>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-1 col-sm-6">
                    <label for="dsnumero" class="required">Nº</label>
                    <input type="text" class="form-control" id="dsnumero" name="dsnumero" placeholder="Nº" required>
                  </div>

                  <div class="col-md-2 col-sm-6">
                    <label for="dsbairro" class="required">Bairro</label>
                    <input type="text" class="form-control" id="dsbairro" name="dsbairro" placeholder="Bairro" required>
                  </div>

                  <div class="col-md-4 col-sm-6">
                    <label for="dscomp">Complemento</label>
                    <input type="text" class="form-control" id="dscomp" name="dscomp" placeholder="Complemento" >
                  </div>

                  <div class="col-md-1 col-sm-6">
                    <label for="dsuf" class="required">Estado</label>
                    <select class='form-control' name="dsuf" id="dsuf" required> </select>
                  </div>

                  <div class="col-md-4 col-sm-6">
                    <label for="dscidade" class="required">Cidade</label>
                    <select class='form-control' name="dscidade" id="dscidade" data-toggle="tooltip" data-placement="top" onmouseover="toolTipCidades(this)" required> </select>
                  </div>
                </div>


                <div class="row">

                  <div class="col-md-3 col-sm-6">
                    <label for="estadocivil" class="required">Estado Civil</label>
                    <?php echo geraComboEstadoCivil("estadocivil", "required"); ?>
                  </div>

                  <div class="col-md-3 col-sm-6">
                    <label for="celular" class="required">DDD + Celular</label>
                    <input type="text" class="form-control" id="celular" name="celular" placeholder="Celular" maxlength="15" required >
                  </div>

                  <div class="col-md-6 mb-2">
                    <label for="dsemail" class="required">E-mail</label>
                    <input type="email" class="form-control" id="dsemail" name="dsemail" placeholder="seu@email.com.br" required >
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-3 col-sm-6">
                    <label for="telefone" class="required">DDD + Telefone</label>
                    <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefone" maxlength="15" required >
                  </div>

                  <div class="col-md-9 col-sm-6">
                    <label for="pessoacontato" class="required">Pessoa de Contato</label>
                    <input type="text" class="form-control" id="pessoacontato" name="pessoacontato" placeholder="Pessoa de Contato" required >
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-3 col-sm-6">
                    <label for="telefoneref" class="required">DDD + Telefone Referência</label>
                    <input type="text" class="form-control" id="telefoneref" name="telefoneref" placeholder="Telefone" maxlength="15" required >
                  </div>

                  <div class="col-md-9 col-sm-6">
                    <label for="pessoacontatoref" class="required">Contato Referência</label>
                    <input type="text" class="form-control" id="pessoacontatoref" name="pessoacontatoref" placeholder="Contato Referência" required >
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                    <label for="nomepai" class="required">Nome do seu Pai</label>
                    <input type="text" class="form-control" id="nomepai" name="nomepai" placeholder="Nome do seu Pai" required>
                  </div>
                  <div class="col-md-6 col-sm-6 mb-2">
                    <label for="nomemae" class="required">Nome de sua Mãe</label>
                    <input type="text" class="form-control" id="nomemae" name="nomemae" placeholder="Nome de sua Mãe" required>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12 col-sm-6">
                    <label for="profissao" class="required">Profissão</label>
                    <input type="text" class="form-control" id="profissao" name="profissao" placeholder="Profissão" required>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-3 col-sm-6">
                    <label for="nrcnh" class="required">Número CNH</label>
                    <input type="text" class="form-control" id="nrcnh" name="nrcnh" placeholder="CNH" required maxlength="11">
                  </div>
                  
                  <div class="col-md-3 col-sm-6">
                    <label for="nrregcnh" class="required">Número Registro CNH</label>
                    <input type="text" class="form-control" id="nrregcnh" name="nrregcnh" placeholder="Registro CNH" required maxlength="11">
                  </div>

                  <div class="col-md-3 col-sm-6">
                    <label for="categoriacnh" class="required">Categoria CNH</label>
                    <select class='form-control' name="categoriacnh" id="categoriacnh">
                      <option value="">Categoria</option>
                      <option value="A">A - Moto</option>
                      <option value="B">B - Automóvel</option>
                      <option value="C">C - Caminhão</option>
                      <option value="D">D - Ônibus</option>
                      <option value="E">E - Veículo Acoplado</option>
                      <option value="AB">AB</option>
                      <option value="AC">AC</option>
                      <option value="AD">AD</option>
                      <option value="AE">AE</option>
                      <option value="ACC">ACC</option>
                      <option value="ACCB">ACCB</option>
                      <option value="ACCC">ACCC</option>
                      <option value="ACCD">ACCD</option>
                      <option value="ACCE">ACCE</option>
                    </select>                  
                  </div>

                  <div class="col-md-3 col-sm-6">
                    <label for="dtvenccnh" class="required">Validade CNH</label>
                    <input type="date" class="form-control" id="dtvenccnh" name="dtvenccnh" placeholder="Validade" required>
                  </div>

                </div>
                
                <div class="row">
                  <div class="col-md-3 col-sm-6">
                    <label for="dtprimeiracnh" class="required">Data Primeira CNH</label>
                    <input type="date" class="form-control" id="dtprimeiracnh" name="dtprimeiracnh" placeholder="1ª CNH" required>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <label for="dtemissaocnh" class="required">Data Emissão CNH</label>
                    <input type="date" class="form-control" id="dtemissaocnh" name="dtemissaocnh" placeholder="1ª CNH" required>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <label for="ufcnh" class="required">UF Emissão CNH</label>
                    <select class='form-control' name="ufcnh" id="ufcnh" required> </select>
                  </div>

                  <div class="col-md-3 col-sm-6">
                    <label for="orgaocnh" class="required">Orgão Emissão CNH</label>
                    <input type="text" class="form-control" id="orgaocnh" name="orgaocnh" placeholder="Orgão CNH" required maxlength="11">
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-3 col-sm-6">
                    <label for="segurancacnh" class="required">Código Segurança</label>
                    <input type="text" class="form-control" id="segurancacnh" name="segurancacnh" placeholder="Código Segurança" required maxlength="11">
                  </div>

                </div>

                <div class="row">
                  <div class="col-md-12 col-sm-6">
                    <button type="button" class="btn btn-secondary float-right" onclick="trocarGuias('pills-proprietario-tab')"> Próximo <i class="fas fa-angle-right"></i> </button>
                  </div>
                </div>

              </div>

            </div>

            <div class="tab-pane fade" id="pills-proprietario" role="tabpanel" aria-labelledby="pills-contact-tab">

              <div class="row">
                <div class="col-md-12 col-sm-6">

                  <label class="lead">Motorista também é proprietário do veículo? </label>
                  
                  <ul class="nav nav-pills mb-3" id="pills-tab-prop" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="pills-propsim-tab" data-toggle="pill" href="#pills-propsim" role="tab" aria-controls="pills-propsim" aria-selected="true">Sim</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="pills-propnao-tab" data-toggle="pill" href="#pills-propnao" role="tab" aria-controls="pills-propnao" aria-selected="false">Não</a>
                    </li>
                  </ul>
                  <div class="tab-content" id="pills-tab-propContent">
                    <div class="tab-pane fade show active" id="pills-propsim" role="tabpanel" aria-labelledby="pills-propsim-tab"></div>
                    
                    <div class="tab-pane fade" id="pills-propnao" role="tabpanel" aria-labelledby="pills-propnao-tab">
                    

                      <ul class="nav nav-pills mb-3" id="pills-tab-propin" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active" id="pills-propjuridica-tab" data-toggle="pill" href="#pills-propjuridica" role="tab" aria-controls="pills-propjuridica" aria-selected="true">Pessoa Jurídica</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="pills-propfisica-tab" data-toggle="pill" href="#pills-propfisica" role="tab" aria-controls="pills-propfisica" aria-selected="false">Pessoa Física</a>
                        </li>
                      </ul>
                      <div class="tab-content" id="pills-tab-propinContent">
                        <div class="tab-pane fade show active" id="pills-propjuridica" role="tabpanel" aria-labelledby="pills-propjuridica-tab">
                          
                          <div class="row">
                            <div class="col-md-3 col-sm-6">
                              <label for="propnrcnpj" class="required">CNPJ</label>
                              <input type="text" class="form-control" id="propnrcnpj" name="propnrcnpj" placeholder="CNPJ" onblur="validaCNPJ(this)">
                            </div>

                            <div class="col-md-9 col-sm-6 mb-3">
                              <label for="proprazaosocial" class="required">Razão Social</label>
                              <input type="text" class="form-control" id="proprazaosocial" name="proprazaosocial" placeholder="Razão Social">
                            </div>

                          </div>

                        </div>
                        
                        <div class="tab-pane fade" id="pills-propfisica" role="tabpanel" aria-labelledby="pills-propfisica-tab">

                          <div class="row">
                            <div class="col-md-3 col-sm-6">
                              <label for="propnrcpf" class="required">CPF</label>
                              <input type="text" class="form-control" id="propnrcpf" name="propnrcpf" placeholder="CPF" onblur="validaCPF(this)">
                            </div>
                            <div class="col-md-9 col-sm-6">
                              <label for="propnome" class="required">Nome</label>
                              <input type="text" class="form-control" id="propnome" name="propnome" placeholder="Nome" >
                            </div>
                          </div>
                          
                          <div class ="row">
                            <div class="col-md-3 col-sm-6">
                              <label for="propnrrg" class="required">RG</label>
                              <input type="text" class="form-control" id="propnrrg" name="propnrrg" placeholder="RG" >
                            </div>
                            
                            <div class="col-md-3 col-sm-6">
                              <label for="propdtemissaorg" class="required">Data de Emissão RG</label>
                              <input type="date" class="form-control" id="propdtemissaorg" name="propdtemissaorg" placeholder="Data de Emissão" >
                            </div>
                            
                            <div class="col-md-3 col-sm-6">
                              <label for="propufemissaorg" class="required">Estado Emissão RG</label>

                              <select class='form-control' name="propufemissaorg" id="propufemissaorg" name="propufemissaorg" > </select>
                            </div>

                            <div class="col-md-3 col-sm-6">
                              <label for="proporgaoemissaorg" class="required">Orgão Emissor</label>
                              <input type="text" class='form-control' name="proporgaoemissaorg" id="proporgaoemissaorg" >
                            </div>

                          </div>

                          <div class="row">

                              <div class="col-md-3 col-sm-6">
                                <label for="propdtnasc" class="required">Data Nascimentto</label>
                                <input type="date" class="form-control" id="propdtnasc" name="propdtnasc" placeholder="Data de Nascimento" max="<?php echo date('Y-m-d',strtotime("-1 days")); ?>">
                              </div>

                              <div class="col-md-1 col-sm-6">
                                <label for="propdscidadenatural" class="required">Natural</label>
                                <select class='form-control' name="propdsufnatural" id="propdsufnatural" > </select>
                              </div>

                              <div class="col-md-8 col-sm-6">
                                <label for="propdscidadenatural">&nbsp;</label>
                                <select class='form-control' name="propdscidadenatural" id="propdscidadenatural" data-toggle="tooltip" data-placement="top" onmouseover="toolTipCidades(this)" > </select>
                              </div>

                          </div>

                          <div class="row">
                            <div class="col-md-3 col-sm-6">
                              <label for="propnrcep" class="required">CEP</label>
                              <input type="text" class="form-control" id="propnrcep" name="propnrcep" placeholder="99.999-99" onblur="carregaDadosCep(this)" >
                            </div>

                            <div class="col-md-9 col-sm-6">
                              <label for="propdsrua" class="required">Rua</label>
                              <input type="text" class="form-control" id="propdsrua" name="propdsrua" placeholder="Rua" >
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-1 col-sm-6">
                              <label for="propdsnumero" class="required">Nº</label>
                              <input type="text" class="form-control" id="propdsnumero" name="propdsnumero" placeholder="Nº" >
                            </div>

                            <div class="col-md-2 col-sm-6">
                              <label for="propdsbairro" class="required">Bairro</label>
                              <input type="text" class="form-control" id="propdsbairro" name="propdsbairro" placeholder="Bairro" >
                            </div>

                            <div class="col-md-4 col-sm-6">
                              <label for="propdscomp">Complemento</label>
                              <input type="text" class="form-control" id="propdscomp" name="propdscomp" placeholder="Complemento" >
                            </div>

                            <div class="col-md-1 col-sm-6">
                              <label for="propdsuf" class="required">Estado</label>
                              <select class='form-control' name="propdsuf" id="propdsuf"> </select>
                            </div>

                            <div class="col-md-4 col-sm-6">
                              <label for="propdscidade" class="required">Cidade</label>
                              <select class='form-control' name="propdscidade" id="propdscidade" data-toggle="tooltip" data-placement="top" onmouseover="toolTipCidades(this)"> </select>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-3 col-sm-6">
                              <label for="propcelular" class="required">DDD + Celular</label>
                              <input type="text" class="form-control" id="propcelular" name="propcelular" placeholder="Celular" maxlength="15" >
                            </div>
                            <div class="col-md-3 col-sm-6">
                              <label for="proptelefone" class="required">DDD + Telefone</label>
                              <input type="text" class="form-control" id="proptelefone" name="proptelefone" placeholder="Telefone" maxlength="15"  >
                            </div>

                            <div class="col-md-6 col-sm-6">
                              <label for="proppessoacontato" class="required">Pessoa de Contato</label>
                              <input type="text" class="form-control" id="proppessoacontato" name="proppessoacontato" placeholder="Pessoa de Contato"  >
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-3 col-sm-6">
                              <label for="proptelefoneref" class="required">DDD + Telefone Referência</label>
                              <input type="text" class="form-control" id="proptelefoneref" name="proptelefoneref" placeholder="Telefone" maxlength="15"  >
                            </div>

                            <div class="col-md-9 col-sm-6">
                              <label for="proppessoacontatoref" class="required">Contato Referência</label>
                              <input type="text" class="form-control" id="proppessoacontatoref" name="proppessoacontatoref" placeholder="Contato Referência"  >
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-6 col-sm-6">
                              <label for="propnomepai" class="required">Nome do Pai</label>
                              <input type="text" class="form-control" id="propnomepai" name="propnomepai" placeholder="Nome do Pai">
                            </div>
                            <div class="col-md-6 col-sm-6 mb-2">
                              <label for="propnomemae" class="required">Nome da Mãe</label>
                              <input type="text" class="form-control" id="propnomemae" name="propnomemae" placeholder="Nome da Mãe">
                            </div>
                          </div>


                        </div>
                      </div>

                    </div>
                  </div>
                    
                
                </div>              
              </div>

              <div class="row">
                <div class="col-md-12 col-sm-6">
                  <button type="button" class="btn btn-secondary" onclick="trocarGuias('pills-motorista-tab')"> <i class="fas fa-angle-left"></i> Voltar </button>
                  <button type="button" class="btn btn-secondary float-right" onclick="trocarGuias('pills-meuveiculo-tab')"> Próximo <i class="fas fa-angle-right"></i> </button>
                </div>
              </div>

            </div>

            <div class="tab-pane fade" id="pills-meuveiculo" role="tabpanel" aria-labelledby="pills-meuveiculo-tab">

              <div class="row">
                <div class="form-group col-md-3 col-sm-6">
                  
                    <label for="nrplaca" class="required">Placa</label>
                    <div class="input-group">
                      <input type="text" class="form-control uppercase input-sm" id="nrplaca" name="nrplaca" onblur="carregaDadosVeiculo()" placeholder="Placa do veículo" required>
                      <span class="btn input-group-addon"><i class="fa fa-car"></i></span>
                    </div>
                  
                </div>
              </div>

              <div class="row">
                <div class="form-group col-md-4 col-sm-6">
                    <label for="vrenavan" class="required">Renavam</label>
                    <input type="text" class="form-control" id="vrenavam" name="vrenavam" required placeholder="RENAVAM">
                </div>
                <div class="form-group col-md-4 col-sm-6">
                    <label for="vchassi" class="required">Chassi</label>
                    <input type="text" class="form-control" id="vchassi" name="vchassi" required placeholder="Chassi">
                </div>

                <div class="form-group col-md-4 col-sm-6">
                  <label for="vdtemissao" class="required">Data Emissão</label>
                  <input type="date" class="form-control" id="vdtemissao" name="vdtemissao" placeholder="Data Emissão" required>
                </div>

              </div>

              <div id="linha-dados-veic" class="row">
                <div class="form-group col-md-4 col-sm-6">
                  <label for="vmodelo" class="required">Marca/Modelo</label>
                  <input type="text" class="form-control" id="vmodelo" name="vmodelo" placeholder="Marca/Modelo" readonly required>
                </div>

                <div class="form-group col-md-3 col-sm-6">
                    <label for="vcidadeuf" class="required">Cidade-UF de registro</label>
                    <input type="text" class="form-control" id="vcidadeuf" name="vcidadeuf" placeholder="Cidade-UF de registro" readonly required>
                </div>
                <div class="form-group col-md-2 col-sm-6">
                    <label for="vano" class="required">Ano Fabricação</label>
                    <input type="text" class="form-control" id="vano" name="vano" placeholder="Ano Fabricação" readonly required>
                </div>
                <div class="form-group col-md-2 col-sm-6">
                    <label for="vanomodelo" class="required">Ano Modelo</label>
                    <input type="text" class="form-control" id="vanomodelo" name="vanomodelo" placeholder="Ano Modelo" readonly required>
                </div>
                <div class="form-group col-md-1 col-sm-6">
                    <label for="vcor" class="required">Cor</label>
                    <input type="text" class="form-control" id="vcor" name="vcor" placeholder="Cor" readonly required>
                </div>

              </div>

              <div class="row">
                
                <div class="form-group col-md-6 col-sm-6">
                  <label for="vcombustivel" class="required">Tipo Combustível</label>
                  <select class='form-control' name="vcombustivel" id="vcombustivel" placeholder="Combustível">
                    <option value="">Combustível</option>
                    <option value="Gasolina">Gasolina</option>
                    <option value="Etanol">Etanol</option>
                    <option value="Flex">Flex</option>
                    <option value="Diesel">Diesel</option>
                    <option value="GNV">GNV</option>
                  </select>
                </div>

                <div class="form-group col-md-6 col-sm-6">
                  <label for="vtipocarroceria" class="required">Tipo Carroceria</label>
                  <select class='form-control' name="vtipocarroceria" id="vtipocarroceria" placeholder="Carroceria">
                    <option value="">Carroceria</option>
                    <option value="Baú">Baú</option>
                    <option value="Grade baixa">Grade baixa (Carga Seca)</option>
                    <option value="Sider">Sider</option>
                    <option value="Refrigerado">Refrigerado</option>
                    <option value="Frigorífico">Frigorífico</option>
                    <option value="Graneleira">Graneleira (Grade Alta)</option>
                    <option value="Prancha">Prancha</option>
                    <option value="Plataforma">Plataforma</option>
                    <option value="Porta Container">Porta Container (Bug)</option>
                    <option value="Munck">Munck</option>
                    <option value="Silo">Silo</option>
                    <option value="Tanque">Tanque</option>
                    <option value="Basculante">Basculante</option>
                    <option value="Cegonha">Cegonha</option>
                    <option value="Canavieira">Canavieira</option>
                    <option value="Florestal">Florestal</option>
                    <option value="Boiadeira">Boiadeira</option>
                    <option value="Poliguindaste">Poliguindaste</option>
                  </select>
                </div>

              </div>

              <div class="row">
                <div class="form-group col-md-6 col-sm-6">
                  <label for="vrntrc" class="required">Nº RNTRC (ANTT/CONDUMOTO)</label>
                  <input type="text" class="form-control" id="vrntrc" name="vrntrc" placeholder="Nº RNTRC" required>
                </div>

                <div class="form-group col-md-6 col-sm-6">
                  <label for="vdtvencrntrc" class="required">Validade RNTRC/CONDUMOTO</label>
                  <input type="date" class="form-control" id="vdtvencrntrc" name="vdtvencrntrc" placeholder="Validade RNTRC" required>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12 col-sm-6">
                  <button type="button" class="btn btn-secondary" onclick="trocarGuias('pills-proprietario-tab')"> <i class="fas fa-angle-left"></i> Voltar </button>
                  <button type="button" class="btn btn-secondary float-right" onclick="trocarGuias('pills-contact-tab')"> Próximo <i class="fas fa-angle-right"></i> </button>
                </div>
              </div>

            </div>
            <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
              
              <div class="row">
                
              <div class="col-md-12 col-sm-6 mt-3">
                <div class="card" >
                  <div class="card-body">
                    <h6 class="card-title">Motorista</h6>
                    <div class="mb-3">
                      <div class="form-group mb-3">
                        <label for="arquivoCNH" class="required">Cópia CNH</label>
                        <input type="file" class="form-control-file" id="arquivoCNH" name="arquivoCNH" accept="image/*" required >

                        <label for="arquivoDocResidencia" class="required mt-3">Cópia Comprovante Residência</label>
                        <input type="file" class="form-control-file" id="arquivoDocResidencia" name="arquivoDocResidencia" accept="image/*" required>
                      </div>

                    </div>
                  </div>
                </div>                
              </div>


              <div class="col-md-12 col-sm-6 mt-3">
                <div class="card">
                  <div class="card-body">
                    <h6 class="card-title" id="card-docs-prop-title">Não é necessário informar, pois o motorista é o Proprietário</h6>
                    <div class="mb-3">
                      <fieldset id="card-docs-prop" class="form-group mb-3" disabled>
                        <label id="LbarquivoCNHProp" for="arquivoCNHProp">Cópia CNH</label>
                        <input type="file" class="form-control-file" id="arquivoCNHProp" name="arquivoCNHProp" accept="image/*" >

                        <br />

                        <label id="LbarquivoDocResidenciaProp" for="arquivoDocResidenciaProp">Cópia Comprovante Residência</label>
                        <input type="file" class="form-control-file" id="arquivoDocResidenciaProp" name="arquivoDocResidenciaProp" accept="image/*">
                      </fieldset>

                    </div>
                  </div>
                </div>                
              </div>


              <div class="col-md-12 col-sm-6 mt-3">
                <div class="card" >
                  <div class="card-body">
                    <h6 class="card-title">Veículo</h6>
                    <div class="mb-3">
                      <div class="form-group mb-3">
                        <label for="arquivoCRLV" class="required">Cópia CRLV</label>
                        <input type="file" class="form-control-file" id="arquivoCRLV" name="arquivoCRLV" accept="image/*" required>

                        <label for="arquivoRNTRC" class="required mt-3">Cópia RNTRC(ANTT/CONDUMOTO)</label>
                        <input type="file" class="form-control-file" id="arquivoRNTRC" name="arquivoRNTRC" accept="image/*" required>
                      </div>

                    </div>
                  </div>
                </div>                
              </div>


                <div class="col-md-12 col-sm-6">
                  <div class="form-group mb-3">
                  </div>
                </div>              

              </div>

              <div class="row">
                <div class="col-md-12 col-sm-6">
                  <button type="button" class="btn btn-secondary" onclick="trocarGuias('pills-meuveiculo-tab')"> <i class="fas fa-angle-left"></i> Voltar </button>

                  <button type="button" class="btn btn-info float-right" onclick="validarCamposForm()"> <i class="far fa-share-square"></i> Enviar Cadastro </button>
                </div>
              </div>

              <div class="row">
                
                <br class="clearfix"> 

                <div class="col-md-12 col-sm-6 mt-2">
                  <div id="campos-obrigatorios" class="alert alert-danger fade" role="alert">
                    Revise os campos marcados com <span class="required"></span> para enviar seu cadastro <i class="far fa-grin-beam-sweat"></i>
                  </div>
                </div>
              </div>

            </div>
          </div>

        </div>


      </div>

    </form>  

  </div>
</body>

</html>
